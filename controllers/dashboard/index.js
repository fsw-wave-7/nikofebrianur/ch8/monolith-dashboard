const { User, Post, Visitor } = require('../../models')

module.exports = {
    home: async (req, res) => {
        const locals = {
            data: [{
                User: await User.count(),
                Post: await Post.count(), 
                Visitor: await Visitor.count(), 
                Reader: await Post.sum('read')
            }],

            contentName: 'Statictic',
            layout: 'layout/dashboard'
        }
        res.render('pages/dashboard/home', locals)
    },
    post: require('./post')
}