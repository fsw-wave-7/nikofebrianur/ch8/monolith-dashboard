const express = require('express')
const app = express()
const { PORT = 8000 } = process.env
const expressLayout = require('express-ejs-layouts')
const morgan = require('morgan')

const setDefault = (req, res, next) => {
    res.locals.contentName = "Default"
    next()
}

app.use(morgan('dev'))

// View engine
app.use(express.static('public'))
app.set('view engine', 'ejs')

/// EJS
app.set('view engine', 'ejs')
app.use(expressLayout)

// Parser
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

app.use(expressLayout)
app.set('layout', 'layout/default')

const router = require('./router')
app.use(router)

app.listen(PORT, () => {
    console.log(`Server ON port:${PORT}`)
})