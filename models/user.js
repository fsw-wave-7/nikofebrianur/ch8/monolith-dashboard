'use strict';
const {
  Model
} = require('sequelize');
const bcrypt = require('bcrypt')
const admin = { 
  id: Math.floor(Math.random() * 100), username: 'admin', password: bcrypt.hashSync('123456', 10)}
  
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    
    static associate(models) {
      // define association here
    }
    // function User({ id, username, password}) {
    //   this.id = id
    //   this.username = username
    //   this.password = password
    //   return this

    // }
  }

  User.prototype.verifyPassword = function(password) {
    return bcrypt.compareSync(password, this.password)
  }

  User.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });

  User.findOne = function({ username }) {
    if (username !== 'admin') return Promise.resolve(null)
    const user = new User(admin)
    return Promise.resolve(user)
  }

  User.findByPk = function(id) {
    if (username !== 'admin') return Promise.resolve(null)
    const user = new User(admin)
    return Promise.resolve(user)
  }
  return User;
}